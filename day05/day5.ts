import fs from 'fs';

interface Instruction {
	number: number;
	from: number;
	to: number;
}

const cargoRows: string[][] = [];
const instructions: Instruction[] = [];

fs.readFileSync('data.txt', {
	encoding: 'utf-8',
})
	.split('\n')
	.forEach((line) => {
		if (line.length === 35) {
			const lineArray = line.split('');
			const cargoRow: string[] = [];
			lineArray.forEach((char, i) => {
				if (
					[1, 5, 9, 13, 17, 21, 25, 29, 33].includes(i) &&
					!/^\d+$/.test(char)
				) {
					cargoRow.push(char);
				}
			});
			if (cargoRow.length) cargoRows.push(cargoRow);
		} else {
			if (line !== '') {
				const lineArray = line.split(' ');
				instructions.push({
					number: parseInt(lineArray[1]),
					from: parseInt(lineArray[3]),
					to: parseInt(lineArray[5]),
				});
			}
		}
		return;
	});

const cargoStacks: string[][] = Array.from(Array(9), () => new Array(0));

cargoRows.forEach((row) => {
	row.forEach((item, i) => {
		if (item !== ' ') cargoStacks[i].push(item);
	});
});

instructions.forEach((instruction) => {
	const cratesToMove = cargoStacks[instruction.from - 1].splice(
		0,
		instruction.number
	);
	cargoStacks[instruction.to - 1] = [
		// ...cratesToMove.reverse(), // for part one
		...cratesToMove, // for part two
		...cargoStacks[instruction.to - 1],
	];
});

console.log(
	cargoStacks.reduce((acc, curr) => {
		return acc + curr[0];
	}, '')
);
