const fs = require('fs');

const elvenCalories = fs.readFileSync('data.txt', {
        encoding: 'utf-8'
    })
    .split(/\n\s*\n/)
    .map(elf => elf.split('\n').reduce((acc, curr) => acc += parseInt(curr), 0))

console.log(Math.max(...elvenCalories));

const topThreeCalories = elvenCalories.sort((a, b) => b - a).slice(0, 3);
const topThreeTotalCalories = topThreeCalories.reduce((acc, curr) => acc += curr, 0)

console.log(topThreeCalories, topThreeTotalCalories);
