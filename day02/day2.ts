import fs from 'fs';

type Identifier = 'A' | 'B' | 'C' | 'X' | 'Y' | 'Z';

interface Item {
	name: string;
	identifiers: Identifier[];
	points: number;
	winsAgainst: string;
	losesAgainst: string;
}

class RockPaperScissors {
	private rock: Item = {
		name: 'rock',
		identifiers: ['A', 'X'],
		points: 1,
		winsAgainst: 'scissors',
		losesAgainst: 'paper',
	};

	private paper: Item = {
		name: 'paper',
		identifiers: ['B', 'Y'],
		points: 2,
		winsAgainst: 'rock',
		losesAgainst: 'scissors',
	};

	private scissors: Item = {
		name: 'scissors',
		identifiers: ['C', 'Z'],
		points: 3,
		winsAgainst: 'paper',
		losesAgainst: 'rock',
	};

	private items: Item[] = [this.rock, this.paper, this.scissors];

	evaluateStrategy1(
		identifier1: Identifier,
		identifier2: Identifier
	): number {
		const item1: Item | undefined = this.items.find((item) =>
			item.identifiers.includes(identifier1)
		);
		const item2: Item | undefined = this.items.find((item) =>
			item.identifiers.includes(identifier2)
		);
		if (!item1 || !item2) return 0;
		if (item1 === item2) {
			return 3 + item2.points;
		}
		return item2.winsAgainst === item1.name
			? item2.points + 6
			: item2.points;
	}

	evaluateStrategy2(identifier: Identifier, result: string): number {
		const item1: Item | undefined = this.items.find((item) =>
			item.identifiers.includes(identifier)
		);
		let item2;
		if (!item1) return 0;
		switch (result) {
			case 'X':
				item2 = this.items.find(
					(item) => item1.winsAgainst === item.name
				);
				break;
			case 'Y':
				item2 = item1;
				break;
			case 'Z':
				item2 = this.items.find(
					(item) => item1.losesAgainst === item.name
				);
			default:
				break;
		}
		if (!item2) return 0;
		if (item1 === item2) {
			return 3 + item2.points;
		}
		return item2.winsAgainst === item1.name
			? item2.points + 6
			: item2.points;
	}
}

const newGame = new RockPaperScissors();

const strategyGuide = fs.readFileSync('data.txt', {
	encoding: 'utf-8',
});

console.log(
	strategyGuide.split('\n').reduce((acc, curr) => {
		const items = curr.split(' ') as Identifier[];
		return (acc += newGame.evaluateStrategy1(items[0], items[1]));
	}, 0)
);
console.log(
	strategyGuide.split('\n').reduce((acc, curr) => {
		const items = curr.split(' ') as Identifier[];
		return (acc += newGame.evaluateStrategy2(items[0], items[1]));
	}, 0)
);
