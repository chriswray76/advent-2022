import fs from 'fs';

const rucksacks = fs
	.readFileSync('data.txt', {
		encoding: 'utf-8',
	})
	.split('\n');

const priorityItems = rucksacks
	.map((rucksack) => {
		const middle = Math.floor(rucksack.length / 2);
		return {
			left: rucksack.substring(0, middle).split(''),
			right: rucksack.substring(middle).split(''),
		};
	})
	.map((rucksack) => {
		let priorityItem = '';
		rucksack.left.forEach((item) => {
			if (rucksack.right.includes(item)) {
				priorityItem = item;
			}
		});
		return priorityItem;
	});

const elfTeams = Array.from(
	{ length: Math.ceil(rucksacks.length / 3) },
	(v, i) => rucksacks.slice(i * 3, i * 3 + 3)
).map((team, i) => {
	let badge = '';
	team[0].split('').forEach((item) => {
		if (
			team[1].split('').includes(item) &&
			team[2].split('').includes(item)
		) {
			badge = item;
		}
	});
	return {
		teamNumber: i + 1,
		rucksacks: team,
		badge: badge,
		priority:
			badge === badge.toLowerCase()
				? badge.charCodeAt(0) - 96
				: badge.charCodeAt(0) - 38,
	};
});

console.log(
	priorityItems.reduce((acc, curr) => {
		if (!curr) return acc;
		return curr === curr.toLowerCase()
			? (acc += curr.charCodeAt(0) - 96)
			: (acc += curr.charCodeAt(0) - 38);
	}, 0)
);

console.log(
	elfTeams.reduce((acc, curr) => {
		return (acc += curr.priority);
	}, 0)
);
