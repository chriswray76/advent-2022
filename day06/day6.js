const fs = require('fs');

const PACKET_LENGTH = 14;

const dataStream = fs
	.readFileSync('data.txt', {
		encoding: 'utf-8',
	})
	.split('');

let packetReceived = 0;

dataStream.forEach((char, i) => {
	if (i < PACKET_LENGTH) return;
	const packetMarkerArray = dataStream.slice(i - PACKET_LENGTH, i);
	const charTally = packetMarkerArray.reduce((acc, curr) => {
		if (acc.hasOwnProperty(curr)) {
			acc[curr]++;
		} else {
			acc[curr] = 1;
		}
		return acc;
	}, new Object());
	if (Object.keys(charTally).length === PACKET_LENGTH && !packetReceived) {
		packetReceived = i;
	}
});

console.log(packetReceived);
