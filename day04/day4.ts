import fs from 'fs';

const cleaningPairs = fs
	.readFileSync('data.txt', {
		encoding: 'utf-8',
	})
	.split('\n')
	.map((pair) => {
		const [firstElf, secondElf] = pair.split(',');
		return {
			elf1: {
				from: parseInt(firstElf.split('-')[0]),
				to: parseInt(firstElf.split('-')[1]),
			},
			elf2: {
				from: parseInt(secondElf.split('-')[0]),
				to: parseInt(secondElf.split('-')[1]),
			},
		};
	});

console.log(
	cleaningPairs.reduce((acc, curr) => {
		const { elf1, elf2 } = curr;
		if (
			(elf1.from >= elf2.from && elf1.to <= elf2.to) ||
			(elf2.from >= elf1.from && elf2.to <= elf1.to)
		) {
			return (acc += 1);
		}
		return acc;
	}, 0)
);

console.log(
	cleaningPairs.reduce((acc, curr) => {
		const { elf1, elf2 } = curr;
		if (
			(elf1.from >= elf2.from && elf1.from <= elf2.to) ||
			(elf2.from >= elf1.from && elf2.from <= elf1.to)
		) {
			return (acc += 1);
		}
		return acc;
	}, 0)
);
