import { dir } from 'console';
import fs from 'fs';

interface File {
	name: string;
	path: string;
	size: number;
}

interface Directory {
	path: string;
	size: number;
}

const commandLine = fs
	.readFileSync('data.txt', {
		encoding: 'utf-8',
	})
	.split('\n');

const files: File[] = [];
const directories: Directory[] = [];
let currentDir = '';

commandLine.forEach((line) => {
	const lineArray = line.split(' ');
	if (lineArray[0] === '$') {
		if (lineArray[1] === 'cd') {
			if (lineArray[2] !== '..') {
				currentDir += `${
					lineArray[2] === '/' ? '/' : '/' + lineArray[2]
				}`;
			} else {
				currentDir = currentDir.substring(
					0,
					currentDir.lastIndexOf('/')
				);
			}
		}
		const directory = directories.find((dir) => dir.path === currentDir);
		if (!directory) {
			directories.push({
				path: currentDir,
				size: 0,
			});
		}
	}
	if (/^\d/.test(lineArray[0])) {
		const size = parseInt(lineArray[0]);
		files.push({
			name: lineArray[1],
			path: currentDir,
			size,
		});
	}
});

directories.forEach((dir) => {
	dir.size = files.reduce((acc, curr) => {
		return curr.path.includes(dir.path) ? (acc += curr.size) : acc;
	}, 0);
});

console.log(
	directories
		.filter((dir) => dir.size <= 100000)
		.reduce((acc, curr) => {
			return acc + curr.size;
		}, 0)
);

// part 2

const DISK_SIZE = 70000000;
const SPACE_NEEDED = 30000000;

const freeSpace = DISK_SIZE - directories[0].size;

console.log(
	directories
		.filter((dir) => dir.size >= SPACE_NEEDED - freeSpace)
		.sort((a, b) => a.size - b.size)[0]
);
